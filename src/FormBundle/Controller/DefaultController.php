<?php

namespace FormBundle\Controller;

use FormBundle\Entity\Tag;
use FormBundle\Entity\Task;
use FormBundle\Form\TaskType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller {

    /**
     * @Route("/")
     */
    public function indexAction(Request $request) {
        $task = new Task();

        // dummy code - this is here just so that the Task has some tags
        // otherwise, this isn't an interesting example
        $tag1 = new Tag();
        // $tag1->setName('tag1');
        // $task->getTags()->add($tag1);
        // end dummy code

        $form = $this->createForm(TaskType::class, $task);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($task);
            $em->flush();
        }

        return $this->render('FormBundle:Default:index.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

}
